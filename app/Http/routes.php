<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# Route::get('/', 'HomeController@index');
Route::get('/', function() {
	return Redirect::route('os.index');
});

Route::get('home', 'HomeController@index');

Route::resource('tarefa', 'TarefaController');

Route::resource('admin', 'Admin\DashboardController');

# O.S. ****
Route::resource('os', 'Os\OsDashboardController');

# R.H.
Route::resource('rh', 'ClientController');
Route::resource('client', 'ClientController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
