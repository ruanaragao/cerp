<?php namespace App\Http\Controllers\Os;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Client;
use App\Os;
use Input, Redirect;

class OsDashboardController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$oss = Os::orderBy('id', 'desc')->get();
		$clients = Client::orderBy('id')->get();
		return view('os.index', compact('oss'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// $os = Os::orderBy('id')->get();
		$clients = Client::orderBy('id')->get(); // pegar list de clientes
		return view('os.create', compact('clients'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$os = Os::create(Input::all());
		return Redirect::route('os.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$os = Os::find($id);
		// return 'id: '.$os->id;
		return view('os.show', compact('os'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$os = Os::find($id);
		$clients = Client::orderBy('id')->get();
		return view('os.edit', compact('os', 'clients'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$osUpdate = Input::all();
		$os = Os::find($id);
		$os->update($osUpdate);
		return Redirect::route('os.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
