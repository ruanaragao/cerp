<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	//
	protected $fillable = array('id', 'name', 'email', 'cp', 'phone1', 'phone2', 'obs', 'address', 'complement', 'city', 'state', 'zip');

}