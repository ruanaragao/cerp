<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Os extends Model {

	protected $fillable = array(
		'id', 
		'client', 
		'service_type', 
		'equipment', 
		'eq_mark', 
		'eq_model', 
		'num_serie', 
		'date_in', 
		'date_prediction', 
		'status', 
		'anex', 
		'obs_equipment', 
		'desc_problem', 
		'desc_service', 
		'obs_internal'
	);

}