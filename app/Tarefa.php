<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model {

	// Campos da tabela
	protected $fillable = array('titulo', 'corpo');

}
