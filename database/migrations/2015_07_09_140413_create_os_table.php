<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('os', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client');
			$table->integer('service_type');
			$table->string('equipment');
			// $table->integer('os_number'); ---> id
			$table->string('eq_mark');
			$table->string('eq_model');
			$table->string('num_serie');
			$table->dateTime('date_in');
			$table->dateTime('date_prediction');
			$table->integer('status');
			$table->string('anex');
			$table->text('obs_equipment');
			$table->text('desc_problem');
			$table->text('desc_service');
			$table->text('obs_internal');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('os');
	}

}