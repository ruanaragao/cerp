- OS
---- DASHBOARD
---- ORDENS DE SERVIÇO
---- OS ABERTAS
---- OS FINALIZADAS
- VENDAS
- ESTOQUE
- FINANCEIRO
- RH


- clients - 
id
name
cp
phone1
phone2
email
obs
adress
cnumero
complemento
token

- os -
id
client
service_type
equipment
os_number
eq_mark
eq_model
num_serie
date_in
date_prediction
anex
obs_equipment
desc_problem
desc_service
obs_internal


















Funções:

* Clientes
-Cadastro de clientes
-Permite editar o cadastro dos clientes
-Permite excluir clientes
-Listar todos os clientes


* OS
-Adicionar Ordem de Serviço
-Localizar Ordem de Serviço (abertas e fechadas)
-Processamento de Ordem de Serviço (Solução)
-Impressão da Ordem de Serviço
-Anexar arquivos

* Produtos
-Cadastro de produtos
-Permite editar o cadastro dos produtos
-Permite inativar produtos
-Listar todos os produtos

* Serviços
-Cadastro de serviços
-Permite editar o cadastro dos serviços
-Permite inativar serviços
-Listar todos os serviços


* Usuários
-Permite cadastrar usuários
-Permite editar usuários
-Permite desativar usuários


* Gerenciamento de Permissões
-Permite adicionar permissão para cada evento dos módulos (Visualizar, Adicionar, Editar, Excluir)
-Permite editar permissão
-Permite desativar permissão


* Relatórios em PDF
-Relatório de Produtos
-Relatórios de Ordens de Serviço
-Relatório de Clientes
-Relatório de Serviços


Serão inseridas as seguintes funções:

- SLA (Controle o tempo dos chamados)

- Mensagem intranet (Converse com sua equipe pelo próprio sistema)

- Painel para clientes