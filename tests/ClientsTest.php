<?php
/**
* Teste de tarefas
*/
class ClientsTest extends TestCase {
	
	public function testeClients() {
		$response = $this->call('GET', '/rh');
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testeClientsList() {
		$response = $this->call('GET', '/clients/list');
		$this->assertEquals(200, $response->getStatusCode());
	}
}