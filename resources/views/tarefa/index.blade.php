@extends('app')

@section('content')
<div class="row">
  <div class="col-md-3">
    ...
  </div>
  <div class="col-md-9">
    <h1>Lista de tarefas <a href="{!!URL::route('tarefa.create')!!}" class="btn btn-default">Nova</a></h1>
    <table class="table table-striped">
      <thead>
        <th>ID</th>
        <th>Título</th>
        <th>Conteúdo</th>
      </thead>
      <tbody>
      <?php foreach ($tarefas as $tarefa): ?>
        <tr>
          <td><?php echo $tarefa->id ?></td>
          <td><?php echo $tarefa->titulo ?></td>
          <td><?php echo $tarefa->corpo ?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</div>
@endsection