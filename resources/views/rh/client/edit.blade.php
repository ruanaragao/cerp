@extends('rh.master')

@section('rh_content')
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Cadastrar novo cliente</h3>
		</div>
		<div class="panel-body">
			{!! Form::model($client, array('route' => array('client.update', $client->id), 'method' => 'PUT')) !!}
	    		<div class="row">
	    			<div class="col-md-8 form-group">
    					<label for="client-name">Nome</label>
    					<input id="client-name" class="form-control" name="name" required="" value="{!! $client->name !!}">
	    			</div>
	    			<div class="col-md-4 form-group">
	    				<label for="client-cpf">CNPJ/CPF</label>
	    				<input id="client-cpf" class="form-control" name="cp" value="{!! $client->cp !!}">
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-md-4 form-group">
    					<label for="client-phone">Telefone para contato</label>
    					<input id="client-phone" class="form-control" name="phone1" value="{!! $client->phone1 !!}">
	    			</div>
	    			<div class="col-md-4 form-group">
	    				<label for="client-ophone">Outro telefone</label>
	    				<input id="client-ophone" class="form-control" name="phone2" value="{!! $client->phone2 !!}">
	    			</div>
	    			<div class="col-md-4 form-group">
	    				<label for="client-email">Email</label>
	    				<input id="client-email" class="form-control" name="email" value="{!! $client->email !!}">
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-md-12 form-group">
    					<label for="client-obs">Observação</label>
    					<textarea id="client-obs" class="form-control" name="obs">{!! $client->obs !!}</textarea>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-md-3 form-group">
	    				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}">
	    				<button id="client-btn-save" class="btn btn-primary" accesskey="s">Salvar</button>
	    				<a href="#" id="client-btn-delete" class="btn btn-danger">Excuir</a>
	    			</div>
	    		</div>
	    	{!! Form::close() !!}
		</div>
	</div>
@stop