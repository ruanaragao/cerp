@extends('rh.master')

@section('rh_content')
  <div class="row">
  	<div class="col-md-6">
  		<h1>Clientes <a href="{!!URL::route('client.create')!!}" class="btn btn-default">Novo</a></h1>
  	</div>
  	<div class="col-md-6 form-group">
		<div class="row form-inline">
			<div class="col-md-12 form-group" style="margin-top: 25px;">
				<input id="src-client" class="form-control" type="text" placeholder="Buscar cliente">
				<button id="client-btn-src" class="btn btn-primary" accesskey="b">Buscar</button>
			</div>
		</div>
  	</div>
  </div>
  <hr>
  <table class="table table-striped">
    <thead>
      <th>Cod.</th>
      <th>Nome</th>
      <th>CNPJ/CPF</th>
      <th>Tel. de contato</th>
      <th>Situação</th>
      <th>Edt.</th>
    </thead>
    <tbody>
    <?php foreach ($clients as $client): ?>
      <tr>
        <td><?php echo $client->id ?></td>
        <td><?php echo $client->name ?></td>
        <td><?php echo $client->cp ?></td>
        <td><?php echo $client->phone1 ?></td>
        <td><?php echo "AGUARDANDO" ?></td>
        <td><a href="{{route('client.edit',$client->id)}}" class="btn btn-warning">Ver</a></td>
      </tr>
      <?php endforeach ?>
    </tbody>
  </table>
@stop