@extends('app')

@section('content')
<div class="row">
  <div class="col-md-3">
  	<nav class="list-group">
      <a href="{!!URL::route('rh.index')!!}" class="list-group-item">Dashboard</a>
      <a href="{!!URL::route('client.index')!!}" class="list-group-item">Clientes</a>
  		<a href="{!!URL::route('client.index')!!}" class="list-group-item">Fornecedores</a>
  	</nav>
  </div>
  <div class="col-md-9">
    @yield('rh_content')
    
  </div>
</div>
@endsection