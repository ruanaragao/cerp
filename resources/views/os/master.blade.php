@extends('app')

@section('content')
<div class="row">
  <div class="col-md-3">
  	<nav class="list-group">
  		<a href="{!!URL::route('os.index')!!}" class="list-group-item">Dashboard</a>
  		<a href="{!!URL::route('os.index')!!}" class="list-group-item">Ordens de Serviço</a>
  		<a href="#" class="list-group-item">OS Abertas</a>
  		<a href="#" class="list-group-item">OS Fechadas</a>
  	</nav>
  </div>
  <div class="col-md-9">
    @yield('os_content')
    
  </div>
</div>
@endsection