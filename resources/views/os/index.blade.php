<?php
function statusOs($value=1)
{
  switch ($value) {
    case 1:
      return "Pendente";
      break;
    case 2:
      return "Cancelado";
      break;
    case 3:
      return "Em andamento";
      break;
    case 4:
      return "Executado";
      break;
    case 5:
      return "Finalizado";
      break;
    
  }
}
?>
@extends('os.master')

@section('os_content')
  <div class="row">
    <div class="col-md-6">
      <h1>O.S. <a href="{!!URL::route('os.create')!!}" class="btn btn-default">Nova</a></h1>
    </div>
    <div class="col-md-6 form-group">
    <div class="row form-inline">
      <div class="col-md-12 form-group" style="margin-top: 25px;">
        {!! Form::open(array('url' => URL::route('os.show', '2'), 'method' => 'post')) !!}
          <input id="src-os" class="form-control" type="text" name="os" placeholder="Buscar O.S.">
          <button id="os-btn-src" class="btn btn-primary" accesskey="b">Buscar</button>
        {!! Form::close() !!}
      </div>
    </div>
    </div>
  </div>
  <hr>
  <table class="table table-striped">
    <thead>
      <th>Nº O.S.</th>
      <th>Equipamento</th>
      <th>Entrada</th>
      <th>Situação</th>
      <th>Opções</th>
    </thead>
    <tbody>
      @foreach ($oss as $os)
      <tr>
        <td>{!! $os->id !!}</td>
        <td>{!! $os->equipment !!}</td>
        <td>{!! $os->created_at !!}</td>
        <td>{!! statusOs($os->status) !!}</td>
        <td><a href="{!! URL::route('os.edit', $os->id) !!}" class="btn btn-warning">Ver</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
@stop