@extends('os.master')

@section('os_content')
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h3 class="panel-title">Nova O.S.</h3>
			</div>
			<div class="panel-body">
		    	{!!Form::open(array('url' => URL::route('os.store'), 'method' => 'post'))!!}
		    		<div class="row">
		    			<div class="col-md-6 form-group">
	    					<label for="os-client">Cliente</label>
	    					<select id="os-client" class="form-control" name="client">
	    						<option>Escolha um cliente ou adicione um novo.</option>
	    						<?php foreach ($clients as $client): ?>
	    						<option value="<?php echo $client->id ?>"><?php echo $client->name ?></option>
	    						<?php endforeach ?>
	    					</select>
		    			</div>
		    			<div class="col-md-2 form-group">
		    				<span style="display:block;margin-top:25px;"></span>
		    				<a href="{!!URL::route('client.create')!!}" class="btn btn-primary">Novo cliente</a>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-tipo">Tipo de serviço</label>
		    				<select id="os-tipo" class="form-control" name="service_type">
		    					<option value="1">Ordem de serviço</option>
		    					<option value="2">Orçamento</option>
		    				</select>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-md-8 form-group">
		    				<label for="os-product">Equipamento recebido</label>
		    				<input id="os-product" class="form-control" type="text" name="equipment"></input>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-cod">Numero da O.S.</label>
		    				<input id="os-cod" class="form-control" type="number" name="id"></input>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<label for="os-mark">Marca do aparelho</label>
		    				<input type="text" id="os-mark" class="form-control" name="eq_mark">
		    			</div>
		    			<div class="col-md-5 form-group">
		    				<label for="os-model">Modelo do aparelho</label>
		    				<input id="os-model" class="form-control" type="text" name="eq_model"></input>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-snumber">Nº de série</label>
		    				<input id="os-snumber" class="form-control" type="text" name="num_serie"></input>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<label for="os-date-in">Data de entrada</label>
		    				<input id="os-date-in" class="form-control" type="date" name="date_in"></input>
		    			</div>
		    			<div class="col-md-3 form-group">
		    				<label for="os-date-prev">Previsão de entrega</label>
		    				<input id="os-date-prev" class="form-control" type="date" name="date_prediction"></input>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-anex">Anexos</label>
		    				<input id="os-anex" type="file" multiple="multiple" name="anex"></input>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Observações sobre o recebimento do equipamento</label>
		    				<textarea id="os-descrip" class="form-control" name="obs_equipment"></textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Descrição do problema ou defeito apresentado</label>
		    				<textarea id="os-descrip" class="form-control" name="desc_problem"></textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Descrição do serviço que será prestado</label>
		    				<textarea id="os-descrip" class="form-control" name="desc_service"></textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Observações internas (Não aparece para o cliente na versão impressa)</label>
		    				<textarea id="os-descrip" class="form-control" name="obs_internal"></textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}">
		    				<button id="os-btn-save" class="btn btn-primary" accesskey="s">Salvar</button>
		    			</div>
		    		</div>
		    	{!!Form::close()!!}
			</div>
		</div>
@stop