@extends('os.master')

@section('os_content')
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h3 class="panel-title">Editar O.S.</h3>
			</div>
			<div class="panel-body">
		    	{!! Form::model($os, array('route' => array('os.update', $os->id), 'method' => 'PUT')) !!}
		    		<div class="row">
		    			<div class="col-md-6 form-group">
	    					<label for="os-client">Cliente</label>
	    					<select id="os-client" class="form-control" name="client">
	    						@foreach($clients as $client)
					            	<option <?php if ( $client->id == $os->client ) { echo 'selected'; } ?> value="{{ $client->id }}">{{ $client->name }}</option>
					          	@endforeach
	    					</select>
		    			</div>
		    			<div class="col-md-2 form-group">
		    				<span style="display:block;margin-top:25px;"></span>
		    				<a href="{!! URL::route('client.create') !!}" class="btn btn-primary">Novo cliente</a>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-tipo">Tipo de serviço</label>
		    				<select id="os-tipo" class="form-control" name="service_type">
		    					<option value="1" <?php if ($os->service_type == 1) { echo 'selected'; } ?>>Ordem de serviço</option>
		    					<option value="2" <?php if ($os->service_type == 2) { echo 'selected'; } ?>>Orçamento</option>
		    				</select>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-md-8 form-group">
		    				<label for="os-product">Equipamento recebido</label>
		    				<input id="os-product" class="form-control" type="text" name="equipment" value="{!! $os->equipment !!}">
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-cod">Nº da O.S.</label>  <span class="label label-danger" title="Só altere este campo caso seja realmente necessário!">Cuidado!</span>
		    				<input id="os-cod" class="form-control" type="number" name="id" value="{!! $os->id !!}">
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<label for="os-mark">Marca do aparelho</label>
		    				<input type="text" id="os-mark" class="form-control" name="eq_mark" value="{!! $os->eq_mark !!}">
		    			</div>
		    			<div class="col-md-5 form-group">
		    				<label for="os-model">Modelo do aparelho</label>
		    				<input id="os-model" class="form-control" type="text" name="eq_model" value="{!! $os->eq_model !!}">
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-snumber">Nº de série</label>
		    				<input id="os-snumber" class="form-control" type="text" name="num_serie" value="{!! $os->num_serie !!}">
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<label for="os-date-in">Data de entrada</label>
		    				<input id="os-date-in" class="form-control" type="date" name="date_in" value="{!! $os->date_in !!}">
		    			</div>
		    			<div class="col-md-3 form-group">
		    				<label for="os-date-prev">Previsão de entrega</label>
		    				<input id="os-date-prev" class="form-control" type="date" name="date_prediction" value="{!! $os->date_prediction !!}">
		    			</div>
		    			<div class="col-md-2 form-group">
		    				<label for="os-status">Situação</label>
		    				<select class="form-control" name="status">
		    					<option value="1" <?php if ($os->status == 1) { echo 'selected'; } ?>>Pendente</option>
		    					<option value="2" <?php if ($os->status == 2) { echo 'selected'; } ?>>Cancelado</option>
		    					<option value="3" <?php if ($os->status == 3) { echo 'selected'; } ?>>Em andamento</option>
		    					<option value="4" <?php if ($os->status == 4) { echo 'selected'; } ?>>Executado</option>
		    					<option value="5" <?php if ($os->status == 5) { echo 'selected'; } ?>>Finalizado</option>
		    				</select>
		    			</div>
		    			<div class="col-md-4 form-group">
		    				<label for="os-anex">Anexos</label>
		    				<a href="{!! $os->anex !!}">Anexo</a>
		    				<input id="os-anex" type="file" multiple="multiple" name="anex">
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Observações sobre o recebimento do equipamento</label>
		    				<textarea id="os-descrip" class="form-control" name="obs_equipment">{!! $os->obs_equipment !!}</textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Descrição do problema ou defeito apresentado</label>
		    				<textarea id="os-descrip" class="form-control" name="desc_problem">{!! $os->desc_problem !!}</textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Descrição do serviço que será prestado</label>
		    				<textarea id="os-descrip" class="form-control" name="desc_service">{!! $os->desc_service !!}</textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-12 form-group">
		    				<label for="os-descrip">Observações internas (Não aparece para o cliente na versão impressa)</label>
		    				<textarea id="os-descrip" class="form-control" name="obs_internal">{!! $os->obs_internal !!}</textarea>
		    			</div>
		    		</div>

		    		<div class="row">
		    			<div class="col-md-3 form-group">
		    				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}">
		    				<button id="os-btn-save" class="btn btn-primary" accesskey="s">Salvar</button>
		    				<a href="{!! URL::route('os.index') !!}" id="os-btn-save" class="btn btn-warning" accesskey="x">Cancelar</a>
		    			</div>
		    			<div class="col-md-6 form-group">
		    				<button id="os-btn-print" class="btn btn-default" accesskey="p">Imprimir</button>
		    				<button id="os-btn-tprint" class="btn btn-default" accesskey="t">Imprimir para técnico</button>
		    			</div>
		    		</div>
		    	{!! Form::close() !!}
			</div>
		</div>
@stop